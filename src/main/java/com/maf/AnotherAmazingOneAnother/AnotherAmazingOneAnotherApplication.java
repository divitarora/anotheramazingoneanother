package com.maf.AnotherAmazingOneAnother;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnotherAmazingOneAnotherApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnotherAmazingOneAnotherApplication.class, args);
	}

}
